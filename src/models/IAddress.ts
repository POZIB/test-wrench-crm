

export default interface IAddress {
   value: string
   unrestricted_value: string
}