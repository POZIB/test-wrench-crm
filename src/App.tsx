import React from 'react';

import AppRouter from './components/AppRouter';

import Layout from './pages/layout';

function App() {
  return (
    <div className="wrapper">
      <Layout >
        <AppRouter />
      </Layout>
    </div>
  );
}

export default App;
