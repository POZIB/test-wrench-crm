import axios from 'axios'


export const API_URL = ""

const $api = axios.create({
      baseURL: API_URL,
      headers: {       
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Token " + process.env.REACT_APP_API_KEY_TOKEN,
      }
})


export default $api