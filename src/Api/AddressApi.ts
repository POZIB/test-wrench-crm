import {AxiosResponse} from 'axios'

import $api from '../http'
import IAddress from '../models/IAddress'


interface IResponseAddress {
   suggestions: IAddress[]
}

export default class AddressApi {
   static async fetchAddress(query: string): Promise<AxiosResponse<IResponseAddress>> {
      return await $api.post<IResponseAddress>(
         'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address',
         {query}
      )
   }
}