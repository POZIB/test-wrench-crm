import React from 'react';

import './style.scss';

interface IButtonHamburger extends React.ButtonHTMLAttributes<HTMLButtonElement>{

}

const ButtonHamburger: React.FC<IButtonHamburger> = ({...props}) => {
   const [active, setActive] = React.useState(false)

   const handlerClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setActive(!active)
      props.onClick && props.onClick(event)
   }

   return (
      <button 
            {...props} 
            className={`hamburger ${active ? 'active' : ''} ${props.className ? props.className : ''}`} 
            type="button"  
            onClick={handlerClick} 
         >
         <span  />
      </button>
   );
};

export default ButtonHamburger;