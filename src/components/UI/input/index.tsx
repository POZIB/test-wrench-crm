import React from 'react';

import './style.scss';

interface IInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
   error?: string
}

const Input:React.FC<IInputProps> = ({
   error, 
   ...props
}) => {

   return (
      <div className='input-wrapper'>
         <input 
            {...props}
            className={`input ${!props.disabled && error ? 'error' : ''} ${props.className || ''}`} 
            type="text" 
         />
         {
            !props.disabled && error && 
            <span className='input_error-message'>
               {error || "Обязательное поле"}
            </span>
         }
      </div>
   );
};

export default Input;