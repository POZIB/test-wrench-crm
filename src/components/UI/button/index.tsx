import React from 'react';

import {ReactComponent as Loader} from '../../../assets/icons/loader.svg'

import './style.scss';

interface IButton extends React.ButtonHTMLAttributes<HTMLButtonElement>{
   icon?: React.ReactNode
   loading?: boolean
}

const Button: React.FC<IButton> = ({icon, loading, ...props}) => {
console.log((icon || loading) && loading ? "a" : 'b');

   return (
      <button {...props} className={`button ${props.className ? props.className : ''}`} >
         {
            (icon || loading) &&
            loading 
            ? <span className='button__icon loader'><Loader/></span>
            : icon && <span className='button__icon'>{icon}</span>
         }
         <span className='button__content'>{props.children}</span>
      </button>
   );
};

export default Button;