import React from 'react';
import { Link } from 'react-router-dom';

import {ReactComponent as Logo} from '../../assets/icons/logo.svg'
import {ReactComponent as Person} from '../../assets/icons/person.svg'
import useWindowSize from '../../hooks/useWindowSize';
import ButtonHamburger from '../UI/buttonHamburger';

import "./style.scss"


interface IHeader {
   onOpenNavigation: (value: React.SetStateAction<boolean>) => void
}

const Header: React.FC<IHeader> = ({onOpenNavigation}) => {
   const {width} = useWindowSize()

   const togglerOpenNavigation = () => {
      onOpenNavigation(prev => !prev)
   }

   return (
      <header className='header'>
         <div className='header__body'>
            <div className='header__block'>
               <div className='item-header' >
                  {
                     width < 768
                     ?  <div className='header__button-hamburger'>
                           <ButtonHamburger onClick={togglerOpenNavigation}/>
                        </div>
                     :  <Link className='item-header__icon icon-logo' to={'/'}>
                           <Logo />
                        </Link>
                  }
                  <Link className='item-header__text text-title' to={'/'}>
                     Wrench CRM
                  </Link>
               </div>
            </div>

            <div className='header__block'>
               <div className='item-header'>
                  <Link className='item-header__icon icon-person' to={'/'}>
                     <Person />
                  </Link>
                  <Link className='item-header__text text-person' to={'/'}>
                     Имя фамилия
                  </Link>
               </div>
            </div>
         </div>
      </header>
   );
};

export default Header;