import React from 'react';

import ItemNavigation from './itemNavigation';
import {itemsNavigation} from './navigation-data';

import "./style.scss"


interface INavigation {
   isOpen: boolean
   onOpen: (value: React.SetStateAction<boolean>) => void
}

const Navigation: React.FC<INavigation>  = ({isOpen, onOpen}) => {

   return (
      <nav className={`navigation ${isOpen ? 'open' : ''}`} role='navigation'>
         <div className='navigation__wrapper'>
            <div className="navigation__header">
               <span >Меню</span>
            </div>
            <ul className='navigation__list'>
            {
               itemsNavigation.map(item => 
                  <ItemNavigation key={item.label} {...item} >
                     {
                        item.children?.map(subitem => 
                           <ItemNavigation 
                              key={subitem.label} 
                              // copy subitem without child
                              {...{...subitem, children: undefined}} //or {...Object.assign({}, subitem, {children: undefined})}
                           />   
                        )
                     }
                  </ItemNavigation>                    
               )
            }
            </ul>
         </div>
      </nav>
   );
};

export default Navigation;