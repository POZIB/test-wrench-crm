import React from 'react';
import { Link } from 'react-router-dom';

import {ReactComponent as Arrow} from '../../../assets/icons/arrowup.svg'

import "./style.scss"

interface ItemNavigation extends React.ButtonHTMLAttributes<HTMLLIElement> {
   label: string,
   icon?: React.ReactNode,
   link?: string,
   children?: React.ReactElement<ItemNavigation> | Array<React.ReactElement<ItemNavigation>>,
}

const ItemNavigation: React.FC<ItemNavigation> = ({icon, label, link = "#", children, ...props}) => {
   const [isOpen, setOpen] = React.useState(false)

   const subItems = React.Children.map(children, (subitem) => {
   if (!React.isValidElement(subitem)) return subitem
      return React.cloneElement(subitem, {...subitem.props, className: 'sub-item'})
   })

   return (
      <li {...props} className={`item-navigation ${props.className ? props.className : ''}`}>
         <Link  to={link} className='item-navigation__body' onClick={() => setOpen(!isOpen)}>
            <span className='item-navigation__icon'>
               {icon}
            </span>
            <span className='item-navigation__label'>
               {label}
            </span>
               {
                  children && 
                  <span className={`item-navigation__arrow-open ${isOpen ? "open" : ''}`}>
                     <Arrow />
                  </span>
               }
         </Link>
         {
            isOpen && 
            <ul className='item-navigation__container-subitems'>
               {subItems}
            </ul>
         }
      </li>
   );
};

export default ItemNavigation;