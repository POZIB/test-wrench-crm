import {ReactComponent as Home} from '../../assets/icons/home.svg'
import {ReactComponent as Search} from '../../assets/icons/search.svg'
import {ReactComponent as Tables} from '../../assets/icons/tables.svg'
import {ReactComponent as Calendar} from '../../assets/icons/calendar.svg'
import {ReactComponent as Location} from '../../assets/icons/location.svg'
import {ReactComponent as Widget} from '../../assets/icons/widget.svg'
import {ReactComponent as Settings} from '../../assets/icons/settings.svg'
import {ReactComponent as SignOut} from '../../assets/icons/signout.svg'
import {ReactComponent as PersonOutline} from '../../assets/icons/personOutline.svg'
import {ReactComponent as Finance} from '../../assets/icons/finance.svg'

interface IItem {
   label: string,
   icon?: React.ReactNode,
   link?: string,
   children?: IItem[],
}

export const itemsNavigation: IItem[] = [
   {label: 'Главная', icon: <Home/>, link: "/"},
   {label: 'Поиск адресов', icon: <Search/>, link: "/address"},
   {label: 'Таблицы', icon: <Tables/>, },
   {label: 'Календарь', icon: <Calendar/>, },
   {label: 'Карты', icon: <Location/>, },
   {label: 'Виджеты', icon: <Widget/>, },
   {label: 'Настройки', icon: <Settings/>, children: [
         {label: "Настройка профиля", icon: <PersonOutline/>, },
         {label: "Управление финансами", icon: <Finance/>, }
      ]
   },
   {label: 'Выход', icon: <SignOut/>, }, 
]


