import Address from "../pages/address"
import Home from "../pages/home"


export interface IRoute {
   path: string
   element: React.ComponentType 
}

export enum RouteNames {
   REDIRECT = '*',
   HOME = '/',
   ADDRESS = '/address'
}

export const routes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: Home },
   { path: RouteNames.HOME, element: Home },
   { path: RouteNames.ADDRESS, element: Address },
]
