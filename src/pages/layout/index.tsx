import React from 'react';

import Header from '../../components/header';
import Navigation from '../../components/navigation';

import "./style.scss"

interface ILayout {
   children: React.ReactNode
}

const Layout: React.FC<ILayout> = ({children}) => {
   const [isOpenNavigation, setIsOpenNavigation] = React.useState(false)

   return (
      <>
         <Header onOpenNavigation={setIsOpenNavigation}/>
         <main className='main'>
            <div className='main__wrapper'>
               <Navigation isOpen={isOpenNavigation} onOpen={setIsOpenNavigation}/>
               <div className='pages-wrapper'>
                  {children}
               </div>
            </div>
         </main>
      </>
   );
};

export default Layout;