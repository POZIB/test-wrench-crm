import React from 'react';

import {ReactComponent as Search} from '../../assets/icons/search.svg'

import Button from '../../components/UI/button'
import Input from '../../components/UI/input'

import AddressApi from '../../Api/AddressApi'
import IAddress from '../../models/IAddress'

import "./style.scss"
import useWindowSize from '../../hooks/useWindowSize';

type FormFields = {
   address: HTMLInputElement
}

const Address = () => {
   const {width} = useWindowSize()
   const [addressess, setAddresses] = React.useState<IAddress[] | null>(null)
   const [loading, setLoading] = React.useState(false)

   const fetchAdderss = async (query: string) => {
      try {
         setLoading(true)
         const res = await AddressApi.fetchAddress(query)
         setAddresses(res.data.suggestions)
      } catch (error) {
         setAddresses([])
         console.log(error);  
      } finally{
         setLoading(false)
      }
   }  

   const handlerSubmit = (event:  React.FormEvent<HTMLFormElement & FormFields>) => {
      event.preventDefault()
   
      const form = event.currentTarget
      const { address } = form

      fetchAdderss(address.value)
   }

   return (
      <div className='page-address'>
         <div className='page-address__wrapper'>
            <div className='page-address__container__title'>
               <h1 className='page-address__title'>Поиск адрессов</h1>
            </div>
            <div className='page-address__container__subtitle'>
               <h3 className='page-address__subtitle'>Введите интересующий вас адрес</h3>
            </div>
            <form className='form-search-address' onSubmit={handlerSubmit} >
               <div className='form-search-address__input'>
                  <Input
                     disabled={loading} 
                     required
                     minLength={3}
                     name='address'
                     placeholder='Введите интересующий вас адрес'
                  />
               </div>
               <div className='form-search-address__button'>
                  <Button
                     disabled={loading} 
                     loading={loading}
                     icon={width > 768 ? <Search /> : undefined} 
                  >
                     Поиск
                  </Button>
               </div>
            </form>
            {
               addressess &&
               <section className='container-addresses'>
                  <div className='container-addresses__title'>
                     <h3>Адреса</h3>
                  </div>
                  <ul className='list-addresses'>
                     {
                        addressess.length 
                        ?  addressess.map(address => 
                              <li key={address.value} className='list-addresses__item'>
                                 <span className='list-addresses__item__content'>{address.value}</span>
                              </li>   
                           )
                        : <div className='list-addresses__empty'>
                              <span>По вашему запросу не были найдены адреса</span>
                           </div>
                     }
                  </ul>
               </section>
            }
         </div>
      </div>
   );
};

export default Address;